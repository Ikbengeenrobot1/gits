# Gits (WIP)

## Introduction

This is a simple commandline user interface for git. Simplifies a few commands I commonly use, like resetting everything to master (and stashing stuff just in case). Or switching to a new feature branch. No other fancy stuff, for that use a nice graphical interface or lookup the git documentation. *at this point it is not well tested and in progress, I am writing this as a personal project, so it might destroy your computer*.

Furthermore I do not recommended this project layout, I chose a one file approach for easy rollout in the environment I work in. For python projects use a recommended structure, split your files and use a setup.py etc.
