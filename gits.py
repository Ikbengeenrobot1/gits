import subprocess
from pathlib import Path
from typing import List
from unittest import TestCase

global shell
global gui


class Gits:
    @classmethod
    def status(cls):
        gui.sidebar(shell.run(f"git status").stdout)
        gui.refresh()

    @classmethod
    def reset(cls, branch: str = "master"):
        reset_commands = [f"git stash --include-untracked",
                          f"git reset --hard",
                          f"git checkout {branch}"
                          f"git fetch",
                          f"git clean -xdf",
                          f"git pull origin"]
        for command in reset_commands:
            shell.run(command)

        shell.run(f"git submodule update --init")

        for command in reset_commands:
            shell.run(f"git submodule foreach {command}")

    @classmethod
    def list(cls):
        gui.sidebar(shell.run(f"git branch --list").stdout)

    @classmethod
    def switch(cls, branch: str):
        shell.run(f"git stash --include-untracked")
        shell.run(f"git checkout -b {branch}").stdout


class Shell:
    def __init__(self, working_dir: Path = Path()):
        self.working_dir = working_dir

    def run(self, command: str, binary_output: bool = False) -> subprocess.CompletedProcess:
        output = subprocess.Popen(command.split(),
                                  cwd=str(self.working_dir),
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE)
        if not binary_output:
            output.stdout = output.stdout.read().decode()
            output.stderr = output.stderr.read().decode()
        # print(output.stderr)
        return output


class Point:
    def __init__(self, x: int = 0, y: int = 0):
        self.x = x
        self.y = y


class GuiCanvas:
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        # Buffer of all text to write
        self.buffer = None
        self._clear_buffer()
        # cursor position in our screen
        self.cursor = Point(0, 0)

    def _clear_buffer(self):
        # Buffer of all text to write
        self.cursor = Point(0, 0)
        self.buffer = [[' ' for _ in range(self.width)] for _ in range(self.height)]

    def print(self, text: str = '\n'):
        # write to Buffer
        for char in text:
            if char == '\n':
                self.cursor.y += 1
                self.cursor.x = 0
            else:
                self.buffer[self.cursor.y][self.cursor.x] = char
                if self.cursor.x >= self.width:
                    self.cursor.y += 1
                self.cursor.x = (self.cursor.x + 1) % self.width


class Gui:
    def __init__(self, width: int = 120, height: int = 14):
        self.width = width
        self.height = height
        self.main_canvas = GuiCanvas(width, height)
        self.sidebar_canvas = GuiCanvas(40, height)

    def refresh(self):
        self.merge_canvas()

        for line in self.main_canvas.buffer:
            print(''.join(line))

        self.main_canvas._clear_buffer()
        self.sidebar_canvas._clear_buffer()

    def merge_canvas(self):
        """ Merge multiple canvases on top of each other.
        Needs some refactoring to remove hard coded stuff and move to canvas class"""
        offset_x = 80
        for y in range(len(self.sidebar_canvas.buffer)):
            for x in range(len(self.sidebar_canvas.buffer[0])):
                self.main_canvas.buffer[y][x + offset_x] = self.sidebar_canvas.buffer[y][x]

    def print(self, text: str = '\n'):
        self.main(text)

    def main(self, text):
        self.main_canvas.print(text)

    def sidebar(self, text):
        self.sidebar_canvas.print(text)

    def menu(self, options: List):
        """
        List options in a menu view and prompt for a choice.

        :param options: A list with elements [('command to type', 'description', 'pointer to method'), ..]
        :return: None
        """
        buffer = ''
        for command, description, method_call in options:
            buffer += f"{command}: {description}\n"

        self.main(buffer)

        input_text = input(">")
        for command, description, method_call in options:
            if command.split()[0] == input_text.split()[0]:
                if len(input_text.split()) == 1:
                    method_call()
                else:
                    method_call(*input_text.split()[1:])
                return
        else:
            print(f"Failed to parse command given.")


# class WorkingDirectory:
#     def __init__(self,path: Path):
#         self.path = path
#
#     def __enter__(self, ):
#         output = Shell.run(f"pushd \"{self.path}\"")
#         if output.returncode != 0:
#             raise Exception(f"Failed to open working directory {self.path}")
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         Shell.run("popd")


class TestCases(TestCase):
    def test_shell_run(self):
        output = Shell.run("git status")
        self.assertEqual(output.stderr, "fatal: not a git repository (or any of the parent directories): .git\n")


if __name__ == '__main__':
    shell = Shell("M:\\tmp")
    gui = Gui()
    while True:
        gui.menu([("status", "Current status", Gits.status),
                  ("list", "List local branches", Gits.list),
                  ("switch [branch_name]", "Switch to a (new) branch", Gits.switch),
                  ("reset", "Clean and reset to latest master", Gits.reset)
                  ])
        gui.refresh()
